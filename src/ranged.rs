/*
Copyright © 2024 - Massimo Gismondi

This file is part of light-ranged-integers.

light-ranged-integers is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

light-ranged-integers is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with light-ranged-integers.  If not, see <http://www.gnu.org/licenses/>.
*/

use crate::op_mode;
use std::marker::PhantomData;

macro_rules! gen_ranged_structs {
    ($i:ty, $typename:ident, $const_typename:ident) => {
        #[derive(Debug, Clone, Copy)]
        #[doc = concat!("A ranged integer between MIN and MAX, encapsulating [", stringify!($i), "] type.\n\n")]
        ///
        /// The library offers Range types, declared in this form
        /// ```rust,ignore
        #[doc= concat!(stringify!($typename), "<MIN, MAX, OpMode>")]
        /// ```
        /// where:
        /// - `MIN` is a lower limit of the interval
        /// - `MAX` is the upper limit of the interval
        /// - `OpMode` selects how to behave when a number goes out of range. Have a
        ///    look at [op_mode] for all available modes.
        /// ```rust
        /// use light_ranged_integers::{RangedU16, op_mode::Panic};
        ///
        /// // Creating a Ranged value of 3, in the interval from 1 to 6.
        /// let n1 = RangedU16::<1,6, Panic>::new(3);
        ///
        /// ```
        /// The interval must be valid, as the MIN limit must be smaller than MAX.
        /// This is checked at compile time.
        /// ```rust,compile_fail
        /// # use light_ranged_integers::RangedU8;
        /// RangedU8::<2,1>::new(2);
        /// ```
        ///
        /// It will fail to compile even if the limits are the same
        /// ```rust,compile_fail
        /// # use light_ranged_integers::RangedU8;
        /// RangedU8::<2,2>::new(2);
        /// ```
        ///
        /// Pay attention that the code at compile time will check only the validity
        /// of the interval, but the check of the provided value fitting the interval is done at runtime.
        /// If you desire to move that check at compile time too, have a look at [Self::new_const()](Self::new_const()) constructor.
        pub struct $typename<const MIN: $i = {<$i>::MIN}, const MAX:$i = {<$i>::MAX}, OpMode=op_mode::Panic>($i, PhantomData<OpMode>) where OpMode: op_mode::OpModeExt;



        // Implement methods common for all modes
        impl<OpMode, const MIN: $i, const MAX:$i> $typename<MIN, MAX, OpMode> where OpMode: op_mode::OpModeExt
        {
            const COMPTIME_RANGE_CHECK: () = assert!(MIN < MAX, "INVALID RANGE. MIN must be smaller than MAX");

            #[doc=concat!("Builds a new [",stringify!($typename),"] checking the limits at runtime")]
            ///
            /// The number MUST be inside the interval.
            /// Will panic if the number is outside the interval.
            ///
            /// It will always panic **INDEPENDENTLY** of OpMode
            pub const fn new(v: $i) -> Self
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                Self::check_in_range(v);
                Self(v, PhantomData)
            }

            /// Applies the OpMode to try to bring the
            /// provided value into its intended range
            pub fn new_adjust(v:$i) -> Self
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                let v = OpMode::bring_in_range(v, MIN, MAX);
                Self::check_in_range(v);
                Self(v, PhantomData)
            }

            /// This constructor will check both range validity and that the value is in the range
            /// at Compile time.
            ///
            /// The `V` number MUST be in range to compile correctly.
            ///
            /// For example, this code will compile just fine:
            /// ```
            #[doc=concat!("use light_ranged_integers::",stringify!($typename),";")]
            #[doc=concat!("let m = ",stringify!($typename),"::<1,4>::new_const::<2>();")]
            /// ```
            ///
            /// But this one will not compile, as 5 is outside of \[1,4\]
            ///  ```rust,compile_fail
            #[doc=concat!("use light_ranged_integers::",stringify!($typename),";")]
            #[doc=concat!("let m = ",stringify!($typename),"::<1,4>::new_const::<5>();")]
            /// ```
            pub const fn new_const<const V: $i>() -> $typename::<MIN, MAX, OpMode>
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                const_ranged::$const_typename::<MIN, MAX, V>::new().as_ranged::<OpMode>()
            }


            #[doc=concat!("Tries to construct a [",stringify!($typename),"]")]
            ///
            /// Checks the range during initialization.
            /// If the value fits the range, returns a new Ranged value,
            /// otherwise returns None
            pub const fn new_try(value: $i) -> Option<Self>
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                if value >= MIN && value <= MAX
                {
                    return Some(Self(value, PhantomData))
                }
                else
                {
                    return None
                }
            }

            /// Returns a tuple containing the (MIN,MAX) interval limits
            pub const fn get_limits(self) -> ($i,$i)
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                (MIN,MAX)
            }

            /// Limits the value to a new minimum and maximum.
            ///
            /// Panics if the value does not fit the new range
            pub const fn limit<const NEW_MIN: $i, const NEW_MAX: $i>(self) -> $typename<NEW_MIN, NEW_MAX, OpMode>
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                $typename::<NEW_MIN, NEW_MAX, OpMode>::new(self.0)
            }

            /// Limits the value to a new minimum and maximum.
            ///
            /// Adjusts the value by appliying the relevant [OpMode](op_mode::OpModeExt)
            /// if the value does not fit the new range
            pub fn limit_adjust<const NEW_MIN: $i, const NEW_MAX: $i>(self) -> $typename<NEW_MIN, NEW_MAX, OpMode>
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                $typename::<NEW_MIN, NEW_MAX, OpMode>::new_adjust(self.0)
            }


            #[doc="Limits the value to a new minimum; keeps the previous maximum value limit."]
            pub const fn limit_min<const NEW_MIN:$i>(self) -> $typename<NEW_MIN, MAX, OpMode>
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                self.limit::<NEW_MIN, MAX>()
            }

            #[doc="Limits the value to a new maximum; keeps the previous minimum value limit."]
            pub const fn limit_max<const NEW_MAX:$i>(self) -> $typename<MIN, NEW_MAX, OpMode>
            {
                let _ = Self::COMPTIME_RANGE_CHECK;
                self.limit::<MIN, NEW_MAX>()
            }

            /// Returns the inner value alone
            pub const fn inner(&self) -> $i
            {
                debug_assert!(self.0>=MIN && self.0<=MAX);
                let _ = Self::COMPTIME_RANGE_CHECK;
                self.0
            }

            /// Create a new Ranged number,
            /// keeping the same interval
            /// but applying the new [OpMode](op_mode)
            pub const fn as_mode<NewOpMode: op_mode::OpModeExt>(self) -> $typename<MIN,MAX,NewOpMode>
            {
                $typename::<MIN,MAX,NewOpMode>(self.0, PhantomData)
            }

            const fn check_in_range(v: $i)
            {
                assert!(v >= MIN && v <= MAX);
            }
        }

        impl<OpMode, const MIN: $i, const MAX:$i> std::cmp::PartialEq<$i> for $typename<MIN, MAX, OpMode> where OpMode: op_mode::OpModeExt
        {
            fn eq(&self, other: &$i) -> bool
            {
                self.0.eq(other)
            }
        }
        impl<OpMode, const MIN: $i, const MAX:$i> std::cmp::PartialEq for $typename<MIN, MAX, OpMode> where OpMode: op_mode::OpModeExt
        {
            fn eq(&self, other: &$typename<MIN, MAX, OpMode>) -> bool
            {
                self.0.eq(&other.0)
            }
        }
        impl<OpMode, const MIN: $i, const MAX:$i> std::cmp::PartialOrd<$i> for $typename<MIN, MAX, OpMode> where OpMode: op_mode::OpModeExt
        {
            fn partial_cmp(&self, other: &$i) -> Option<std::cmp::Ordering>
            {
                self.0.partial_cmp(other)
            }
        }
        impl<OpMode, const MIN: $i, const MAX:$i> std::cmp::PartialOrd for $typename<MIN, MAX, OpMode> where OpMode: op_mode::OpModeExt
        {
            fn partial_cmp(&self, other: &$typename<MIN, MAX, OpMode>) -> Option<std::cmp::Ordering>
            {
                self.0.partial_cmp(&other.0)
            }
        }

        impl<OpMode, const MIN: $i, const MAX:$i> From<$i> for $typename<MIN,MAX,OpMode>
            where OpMode: op_mode::OpModeExt
        {
            fn from(value: $i) -> Self {
                Self::new(value)
            }
        }

        impl<OpMode, const MIN: $i, const MAX:$i> std::fmt::Display for $typename<MIN,MAX,OpMode>
            where OpMode: op_mode::OpModeExt
        {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!{f, "{}", self.inner()}
            }
        }

        
        // --------------------------------------
        // Implementation for external libraries
        // Serde
        #[cfg(feature = "serde")]
        impl<OpMode, const MIN: $i, const MAX:$i> serde::Serialize for $typename<MIN, MAX, OpMode>
        where OpMode: op_mode::OpModeExt
        {
            fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
                where S: serde::Serializer
            {
                self.inner().serialize(serializer)
            }
        }
        #[cfg(feature = "serde")]
        impl<'de, OpMode, const MIN: $i, const MAX:$i> serde::Deserialize<'de> for $typename<MIN, MAX, OpMode>
        where OpMode: op_mode::OpModeExt
        {
            fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
                where D: serde::Deserializer<'de>
            {
                let value: $i = <$i>::deserialize(deserializer)?;
                if let Some(r) = Self::new_try(value)
                {
                    return Ok(r);
                }
                else
                {
                    return Err(<D::Error as serde::de::Error>::invalid_value(
                        serde::de::Unexpected::Other("int"),
                        &format!("Value {} is not in the desired range [{},{}]", value, MIN, MAX).as_ref()
                    ));
                }
            }
        }
        // Airone
        #[cfg(feature = "airone")]
        impl<OpMode, const MIN: $i, const MAX:$i> airone::serde::SerializableField
            for $typename<MIN, MAX, OpMode>
            where OpMode: 'static+ op_mode::OpModeExt
        {
            fn serialize_field(&self) -> airone::serde::SerializedFieldValue
            {
                airone::serde::SerializedFieldValue::new(
                    self.0.to_string()
                )
            }
            fn deserialize_field(v: airone::serde::SerializedFieldValue) -> Result<Self, airone::error::Error>
            {
                let n: $i = airone::serde::SerializableField::deserialize_field(v)?;
                use airone::error::ParseError;
                use airone::error::Error;
                Self::new_try(n).ok_or(
                    Error::ParseError(
                        ParseError::new(
                            format!("Value should be in range [{}, {}]", MIN, MAX),
                            format!("found {n}")
                        )
                    )
                )
            }
        }
    };
    }

gen_ranged_structs!(u8, RangedU8, ConstRangedU8);
gen_ranged_structs!(u16, RangedU16, ConstRangedU16);
gen_ranged_structs!(u32, RangedU32, ConstRangedU32);
gen_ranged_structs!(u64, RangedU64, ConstRangedU64);
gen_ranged_structs!(i8, RangedI8, ConstRangedI8);
gen_ranged_structs!(i16, RangedI16, ConstRangedI16);
gen_ranged_structs!(i32, RangedI32, ConstRangedI32);
gen_ranged_structs!(i64, RangedI64, ConstRangedI64);

// OPERATIONS IMPLEMENTATION
mod wrap
{
    use super::*;

    macro_rules! gen_add {
        ($i:ty, $bigger:ty, $typename:ident) => {
            impl<const MIN: $i, const MAX: $i> std::ops::Add<$i>
                for $typename<MIN, MAX, op_mode::Wrap>
            {
                type Output = Self;
                fn add(self, rhs: $i) -> Self::Output
                {
                    // KISS "keep it simple stupid"
                    // there was a mathematically cool but longer implementation
                    // Now I simply use a bigger type to avoid overflows
                    // It still uses less variables than the "cool" version
                    let min: $bigger = MIN as $bigger;
                    let max: $bigger = MAX as $bigger;
                    let interval_size: $bigger = max - min + 1;
                    let rhs: $bigger = (rhs as $bigger).rem_euclid(interval_size);

                    let res: $i = <$i>::try_from(
                        ((((self.inner() as $bigger) - min) + rhs) % interval_size) + min
                    )
                    .unwrap();

                    return $typename(res, PhantomData);
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Sub<$i>
                for $typename<MIN, MAX, op_mode::Wrap>
            {
                type Output = Self;
                fn sub(self, rhs: $i) -> Self::Output
                {
                    // KISS "keep it simple stupid"
                    // there was a mathematically cool but longer implementation
                    // Now I simply use a bigger type to avoid overflows
                    // It still uses less variables than the "cool" version
                    let min: $bigger = MIN as $bigger;
                    let max: $bigger = MAX as $bigger;
                    let interval_size: $bigger = max - min + 1;
                    let rhs: $bigger = (rhs as $bigger).rem_euclid(interval_size);

                    // This is exactly the code for Addition, but
                    // we use the modular arithmetic trick to
                    // convert the Subtraction to an Addition
                    // self - rhs (mod L) ≡ self + (L-rhs) (mod L)
                    let res: $i = <$i>::try_from(
                        ((((self.inner() as $bigger) - min) + (interval_size - rhs))
                            % interval_size)
                            + min
                    )
                    .unwrap();
                    return $typename(res, PhantomData);
                }
            }
        };
    }

    gen_add!(u8, u16, RangedU8);
    gen_add!(u16, u32, RangedU16);
    gen_add!(u32, u64, RangedU32);
    gen_add!(u64, u128, RangedU64);
    gen_add!(i8, i16, RangedI8);
    gen_add!(i16, i32, RangedI16);
    gen_add!(i32, i64, RangedI32);
    gen_add!(i64, i128, RangedI64);
}

mod panic
{
    use super::*;
    macro_rules! gen_panic_impl {
        ($i:ty, $typename:ident) => {
            // Additions
            impl<const MIN: $i, const MAX: $i> std::ops::Add for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn add(self, rhs: $typename<MIN, MAX, op_mode::Panic>) -> Self::Output
                {
                    self + rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Add<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn add(self, rhs: $i) -> Self::Output
                {
                    Self::new(self.inner().checked_add(rhs).unwrap())
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::AddAssign
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn add_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Panic>)
                {
                    *self += rhs.inner();
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::AddAssign<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn add_assign(&mut self, rhs: $i)
                {
                    *self = Self::new(self.inner().checked_add(rhs).unwrap());
                }
            }

            // Subtraction
            impl<const MIN: $i, const MAX: $i> std::ops::Sub for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn sub(self, rhs: $typename<MIN, MAX, op_mode::Panic>) -> Self::Output
                {
                    self - rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Sub<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn sub(self, rhs: $i) -> Self::Output
                {
                    Self::new(self.inner().checked_sub(rhs).unwrap())
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::SubAssign
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn sub_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Panic>)
                {
                    *self -= rhs.inner();
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::SubAssign<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn sub_assign(&mut self, rhs: $i)
                {
                    *self = Self::new(self.inner().checked_sub(rhs).unwrap());
                }
            }

            // Multiplication
            impl<const MIN: $i, const MAX: $i> std::ops::Mul for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn mul(self, rhs: $typename<MIN, MAX, op_mode::Panic>) -> Self::Output
                {
                    self * rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Mul<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn mul(self, rhs: $i) -> Self::Output
                {
                    Self::new(self.inner().checked_mul(rhs).unwrap())
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::MulAssign
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn mul_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Panic>)
                {
                    *self = Self::new(self.inner().checked_mul(rhs.inner()).unwrap());
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::MulAssign<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn mul_assign(&mut self, rhs: $i)
                {
                    *self = Self::new(self.inner().checked_mul(rhs).unwrap());
                }
            }

            // Division
            impl<const MIN: $i, const MAX: $i> std::ops::Div for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn div(self, rhs: $typename<MIN, MAX, op_mode::Panic>) -> Self::Output
                {
                    self / rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Div<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                type Output = Self;
                fn div(self, rhs: $i) -> Self::Output
                {
                    Self::new(self.inner().checked_div(rhs).unwrap())
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::DivAssign
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn div_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Panic>)
                {
                    *self = Self::new(self.inner().checked_div(rhs.inner()).unwrap());
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::DivAssign<$i>
                for $typename<MIN, MAX, op_mode::Panic>
            {
                fn div_assign(&mut self, rhs: $i)
                {
                    *self = Self::new(self.inner().checked_div(rhs).unwrap());
                }
            }
        };
    }

    gen_panic_impl!(u8, RangedU8);
    gen_panic_impl!(u16, RangedU16);
    gen_panic_impl!(u32, RangedU32);
    gen_panic_impl!(u64, RangedU64);
    gen_panic_impl!(i8, RangedI8);
    gen_panic_impl!(i16, RangedI16);
    gen_panic_impl!(i32, RangedI32);
    gen_panic_impl!(i64, RangedI64);
}

mod clamp
{
    use super::*;
    macro_rules! gen_clamp_impl {
        ($i:ty, $typename:ident) => {
            // Additions
            impl<const MIN: $i, const MAX: $i> std::ops::Add for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn add(self, rhs: $typename<MIN, MAX, op_mode::Clamp>) -> Self::Output
                {
                    self + rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Add<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn add(self, rhs: $i) -> Self::Output
                {
                    Self::new_adjust(self.inner().saturating_add(rhs))
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::AddAssign
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn add_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Clamp>)
                {
                    *self = *self + rhs;
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::AddAssign<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn add_assign(&mut self, rhs: $i)
                {
                    *self = Self::new_adjust(self.inner().saturating_add(rhs));
                }
            }

            // Subtraction
            impl<const MIN: $i, const MAX: $i> std::ops::Sub for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn sub(self, rhs: $typename<MIN, MAX, op_mode::Clamp>) -> Self::Output
                {
                    self - rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Sub<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn sub(self, rhs: $i) -> Self::Output
                {
                    Self::new_adjust(self.inner().saturating_sub(rhs))
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::SubAssign
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn sub_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Clamp>)
                {
                    *self -= rhs.inner();
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::SubAssign<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn sub_assign(&mut self, rhs: $i)
                {
                    *self = Self::new_adjust(self.inner().saturating_sub(rhs));
                }
            }

            // Multiplication
            impl<const MIN: $i, const MAX: $i> std::ops::Mul for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn mul(self, rhs: $typename<MIN, MAX, op_mode::Clamp>) -> Self::Output
                {
                    self * rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Mul<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn mul(self, rhs: $i) -> Self::Output
                {
                    Self::new_adjust(self.inner().saturating_mul(rhs))
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::MulAssign
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn mul_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Clamp>)
                {
                    *self = *self * rhs;
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::MulAssign<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn mul_assign(&mut self, rhs: $i)
                {
                    *self = *self * rhs;
                }
            }

            // Division
            impl<const MIN: $i, const MAX: $i> std::ops::Div for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn div(self, rhs: $typename<MIN, MAX, op_mode::Clamp>) -> Self::Output
                {
                    self / rhs.inner()
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::Div<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                type Output = Self;
                fn div(self, rhs: $i) -> Self::Output
                {
                    Self::new_adjust(self.inner().saturating_div(rhs))
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::DivAssign
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn div_assign(&mut self, rhs: $typename<MIN, MAX, op_mode::Clamp>)
                {
                    *self = *self / rhs;
                }
            }
            impl<const MIN: $i, const MAX: $i> std::ops::DivAssign<$i>
                for $typename<MIN, MAX, op_mode::Clamp>
            {
                fn div_assign(&mut self, rhs: $i)
                {
                    *self = *self / rhs;
                }
            }
        };
    }

    gen_clamp_impl!(u8, RangedU8);
    gen_clamp_impl!(u16, RangedU16);
    gen_clamp_impl!(u32, RangedU32);
    gen_clamp_impl!(u64, RangedU64);
    gen_clamp_impl!(i8, RangedI8);
    gen_clamp_impl!(i16, RangedI16);
    gen_clamp_impl!(i32, RangedI32);
    gen_clamp_impl!(i64, RangedI64);
}

/// Compile time checked ranges
mod const_ranged
{
    use super::*;
    use crate::op_mode;
    macro_rules! gen_const_ranged_structs {
        ($i:ty, $typename:ident, $const_typename:ident) => {
            #[derive(Clone, Copy)]
            #[doc = concat!("A compile-time checked [",stringify!($typename),"] struct.\n\n")]
            pub struct $const_typename<const MIN: $i, const MAX: $i, const V: $i>;
            impl<const MIN: $i, const MAX: $i, const V: $i> $const_typename<MIN, MAX, V>
            {
                const RANGE_VALID: () = assert!(
                    MIN < MAX,
                    "The range is not valid. MIN must be smaller than MAX"
                );
                const IS_IN_RANGE: () = assert!(
                    V >= MIN && V <= MAX,
                    "Provided value V is outside of the range"
                );

                /// Creates a new compile-checked range number.
                /// The number MUST be in range to compile correctly,
                /// independently of the [OpMode](op_mode)
                pub const fn new() -> Self
                {
                    let _ = Self::RANGE_VALID;
                    let _ = Self::IS_IN_RANGE;
                    return $const_typename::<MIN, MAX, V>;
                }

                /// Converts to a runtime Ranged value, assigning the provided OpMode
                pub const fn as_ranged<OpMode: op_mode::OpModeExt>(
                    &self
                ) -> $typename<MIN, MAX, OpMode>
                {
                    return $typename::<MIN, MAX, OpMode>(V, PhantomData);
                }
            }
        };
    }

    gen_const_ranged_structs!(u8, RangedU8, ConstRangedU8);
    gen_const_ranged_structs!(u16, RangedU16, ConstRangedU16);
    gen_const_ranged_structs!(u32, RangedU32, ConstRangedU32);
    gen_const_ranged_structs!(u64, RangedU64, ConstRangedU64);
    gen_const_ranged_structs!(i8, RangedI8, ConstRangedI8);
    gen_const_ranged_structs!(i16, RangedI16, ConstRangedI16);
    gen_const_ranged_structs!(i32, RangedI32, ConstRangedI32);
    gen_const_ranged_structs!(i64, RangedI64, ConstRangedI64);
}

// // Shortcuts
// #[doc = concat!("A ranged integer between the provided MIN and [", $i ,"::MAX].\n\n")]
// pub type [<Ranged $i:upper Min>]<const MIN:$i, OpMode=op_mode::Panic> = [<Ranged $i:upper>]<MIN, {$i::MAX}, OpMode>;
// #[doc = "A ranged integer between [" $i "::MIN] and the provided MAX.\n\n"]
// pub type [<Ranged $i:upper Max>]<const MAX:$i, OpMode=op_mode::Panic> = [<Ranged $i:upper>]<{$i::MIN}, MAX, OpMode>;
